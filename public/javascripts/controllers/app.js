var module = ons.bootstrap('my-app', ['onsen',
                                      'ngStorage'
                                      ]); 

 
//-----------------------------------------------------------------------------------------//
//    	Directivas
//-----------------------------------------------------------------------------------------//

// lodash support
module.constant('_', window._);

 
 
//-----------------------------------------------------------------------------------------//
//    	controller
//-----------------------------------------------------------------------------------------//

module.controller('AppController', function($scope, 
                                            $timeout,
                                            $localStorage,
                                            $http,
                                            _
                                            ) {

// lodash support
$scope._ = _;

 
    
  
  
//*****************************************************************************************************************//
//    	Propriedades
//*****************************************************************************************************************//
    
//---------------------------------//
//    	ng-Storage
//---------------------------------//
    
$scope.$storage = $localStorage.$default({
	
	items : [],
	index : 0,
    // Dados do novo usuario para cadastro 
	Item: {
			veiculo : '',
			marca: '',
			ano: '',
			vendido:false
	}, 
    
});




//------------------------------------//
//			Variables Msj
//------------------------------------// 

$scope.aviso = [];
    
     
 
//------------------------------------//
//        Metodos dialog, alerts
//------------------------------------//

    // alert geral para notificar que uma operacão teve erro
    $scope.alert = function(msj) {
		ons.notification.alert({ 'title':msj.titulo , 'message': msj.mensaje });
    }
    
    // função que mostra o dialog
    $scope.dialogLoadShow = function(dialog,mensaje){
        // aloca o mensagem a mostrar no alert
        $timeout(function() {
            $scope.$apply(function(){ 
			
                     $scope.$storage.mensaje = mensaje;
                     
                     // mostra o dialog
                     document.getElementById(dialog).show();
            });
        },0);                 
        
    };
    
	// função que oculta o dialog
    $scope.dialogLoadHide = function(dialog){
		// muestra el dialog
        $timeout(function() {
            $scope.$apply(function(){ 
		        document.getElementById(dialog).hide();
            });
        },0); 
    };

//------------------------------------//
//        Metodos de validacão do Ano
//------------------------------------//
	$scope.$storage.errorAno = false;
	$scope.validateAno = function(){
		 
		if (angular.isDefined($scope.$storage.Item) && 
			angular.isDefined($scope.$storage.Item.ano)) {
			// entre 1900 e año atual	
			if($scope.$storage.Item.ano < 1900 || $scope.$storage.Item.ano > (new Date()).getUTCFullYear()) {
				 
		       $scope.$storage.errorAno = true;
            
			}
			else if($scope.$storage.Item.ano.length <= 4) { // anu tenha 4 digitos

		       $scope.$storage.errorAno = true;
          
			}
				
			else{ // Não Error
		       $scope.$storage.errorAno = false;
			}
				
		}
		
	}
	
	
//------------------------------------//
//			GetAll veiculos
//------------------------------------// 

 
// função que obtem tudos os veiculos
$scope.getAllVeiculo = function(){
	
	// Mostramos Icono de Procesamento
    $scope.dialogLoadShow('dialog-1',':)');
	
	$http.get("/veiculos")
	.then( function (data) {	// Sem Error desde o Servidor
		
	    // Escondemos Icono de Procesamento
		$scope.dialogLoadHide('dialog-1');
		
		console.log('data server' , data);
		
		// armazena os dados
		$scope.$storage.items = data.data.items;
		
	
	},
	function (err) {	// Com Error desde o Servidor  
	    
	    // fecha o modal
		$scope.dialogLoadHide('myModal');
		
		// fecha Icono de Procesamento
		$scope.dialogLoadHide('dialog-1');
		
		// Carregamos mensagem para o alert
		$scope.aviso = {};
		$scope.aviso.titulo = 'Error';
		$scope.aviso.mensaje = 'Nao se pode obter os dados';
		
		// Mostra alert
		$scope.alert($scope.aviso);
	    
	});
		
}

// get all veiculos
$scope.getAllVeiculo();
	
//------------------------------------//
//        Metodos ADD, DELETE, APDATE, GET
//------------------------------------//

	// função que Carrega o modal para inserir um novo veiculo
	$scope.newVeiculo = function(){
		// initializa variable
		$scope.$storage.Item = {
				vendido: false
		};
		
		// operacão a ser feita
		$scope.operation = 'Ins';
		
		// mostra modal
		$scope.dialogLoadShow('myModal');
	}
	
	// função que Carrega o modal para atualizar um veiculo
	$scope.updVeiculo = function(itemSelect){
		// carrega dados o veiculo a atulizar
		$scope.$storage.Item = itemSelect;
		
		// operacão a ser feita
		$scope.operation = 'Upd';
		
		// mostra modal
		$scope.dialogLoadShow('myModal');
		
		
	}
	
	
	// função que obtem um veiculo desde o servidor
	$scope.getVeiculo = function(id,index){
			// valida que venha o ID
			if (id) {
				
				// index do item
				$scope.$storage.index = index;
		
				// Mostramos Icono de Procesamento
    			$scope.dialogLoadShow('dialog-1',':)');
		
				var requestUrl = '/veiculos/' + id ;
				
				$http.get(requestUrl)
				.then( function (data) {	// Sem Error desde o Servidor
					
                     // fechamos Icono de Procesamento
					$scope.dialogLoadHide('dialog-1');
					
					
					// armazena o dados do veiculo
					$scope.itemSelect = data.data.item;
					
					
				 
                
				},
			    function (err) {	// Com Error desde o Servidor
                    
                    
					// fechamos Icono de Procesamento
					$scope.dialogLoadHide('dialog-1');
					
					// Carregamos mensagem para o alert
					$scope.aviso = {};
					$scope.aviso.titulo = 'Error';
					$scope.aviso.mensaje = 'Id Invalida';
					
					// Mostramos alert
					$scope.alert($scope.aviso);
                    
				});
			}
			else{
				// Carregamos mensagem para o alert
				$scope.aviso = {};
				$scope.aviso.titulo = 'Error';
				$scope.aviso.mensaje = 'Id Invalida';
				
				// Mostramos alert
				$scope.alert($scope.aviso);
			}
		
	}
	
	// função que obtem os veiculos desde um string ('Marca' inserida pelo usuario)
	$scope.findVeiculo = function(find){
		// valida que venha a 'Marca' a ser pesquisada	
		if (find) {
	
			// Mostramos Icono de Procesamento
			$scope.dialogLoadShow('dialog-1',':)');
	
			var requestUrl = '/veiculos/find';
			
			$http.get(requestUrl, {params: {q : find}})
			.then( function (data) {	// Sem Error desde o Servidor
				
                 // fechamos Icono de Procesamento
				$scope.dialogLoadHide('dialog-1');
				
				// armazena os dados obtidos
				$scope.$storage.items = data.data.items;
			 
            
			},
		    function (err) {	// Com Error desde o Servidor
                
               
				// fechamos Icono de Procesamento
				$scope.dialogLoadHide('dialog-1');
				
				// Carregamos mensagem para o alert
				$scope.aviso = {};
				$scope.aviso.titulo = 'Error';
				$scope.aviso.mensaje = 'Id Invalida';
				
				// Mostramos o alert
				$scope.alert($scope.aviso);
                
			});
		}
		else{
			// get all veiculos
			$scope.getAllVeiculo();
		}
		
	}

	// função que inseri um novo veiculo
	$scope.addVeiculo = function(){
			// valida os dados preenchidos pelo usuario
			if (angular.isDefined($scope.$storage.Item) && 
			angular.isDefined($scope.$storage.Item.ano) &&
			angular.isDefined($scope.$storage.Item.marca) &&
			angular.isDefined($scope.$storage.Item.descricao) &&
			angular.isDefined($scope.$storage.Item.veiculo)) {
		
				// Mostramos Icono de Procesamento
    			$scope.dialogLoadShow('dialog-1',':)');
		
				$http.post("/veiculos", $scope.$storage.Item)
				.then( function (data) {	// Sem Error desde o Servidor
					
                    // fechamos Icono de Procesamento
        			$scope.dialogLoadHide('dialog-1');
					
					
					//insere o novo item
					$scope.$storage.items.push(data.data.item);
					
					// fecha o modal
					$scope.dialogLoadHide('myModal');
				 
                
				},
			    function (err) {	// Com Error desde o Servidor
                    
                    // fecha o modal
					$scope.dialogLoadHide('myModal');
					
					// fechamos Icono de Procesamento
					$scope.dialogLoadHide('dialog-1');
					
					// Carregamos mensagem para o alert
					$scope.aviso = {};
					$scope.aviso.titulo = 'Error';
					$scope.aviso.mensaje = 'Error tentando Inserir';
					
					// Mostramos o alert
					$scope.alert($scope.aviso);
                    
				});
			}
		
	}
	
	
	// função que mostra um alert para confirmar se quer eliminar um veiculo
    $scope.confirmDeleteVeiculo = function(id) {
    	
		ons.notification.confirm( {message:'Eliminar Veiculo? ' })
		.then(
			function(idx) {
				switch(idx) {
					case 0:                
						break;
					case 1: 
						// sim quer eliminar se chama a funcão de eliminacão
						$scope.deleteVeiculo(id);
						break;
				}
			}
		);         
    }
	
	// função que elimina veiculo
	$scope.deleteVeiculo = function(id){
			
			// valida a ID a eliminar
			if (id) {
		
				// Mostramos Icono de Procesamento
    			$scope.dialogLoadShow('dialog-1',':)');
		
				var requestUrl = '/veiculos/' + id ;
				
				$http.delete(requestUrl)
				.then( function (data) {	// Não Error desde o Servidor
					
                     // Fechamos Icono de Procesamento
					$scope.dialogLoadHide('dialog-1');
					
					console.log('data server' , data);
					
					// Eliminamos Veiculo
				    $scope.$storage.items.splice($scope.$storage.index, 1);
				    
				    // limpa o item 
				    $scope.itemSelect = {};
				 
                
				},
			    function (err) {	// Com Error desde o Servidor
					
					// Escondemos Icono de Procesamento
					$scope.dialogLoadHide('dialog-1');
					
					// Carregamos mensagem para o alert
					$scope.aviso = {};
					$scope.aviso.titulo = 'Error';
					$scope.aviso.mensaje = 'Id Invalida';
					
					// Mostramos alert
					$scope.alert($scope.aviso);
                    
				});
			}
			else{
				// Carregamos mensagem para o alert
				$scope.aviso = {};
				$scope.aviso.titulo = 'Error';
				$scope.aviso.mensaje = 'Id Invalida';
				
				// Mostramos alert
				$scope.alert($scope.aviso);
			}
		
	}
	
	// função que atualiza veiculo
	$scope.updateVeiculo = function(){
			// valida que os dados preenchidos pelo usuario existam
			if (angular.isDefined($scope.$storage.Item) && 
			angular.isDefined($scope.$storage.Item.ano) &&
			angular.isDefined($scope.$storage.Item.marca) &&
			angular.isDefined($scope.$storage.Item.descricao) &&
			angular.isDefined($scope.$storage.Item.veiculo)) {
		
				// Mostramos Icono de Procesamento
    			$scope.dialogLoadShow('dialog-1',':)');
    			
    			var requestUrl = '/veiculos/' + $scope.$storage.Item._id ;
		
				$http.put(requestUrl, $scope.$storage.Item)
				.then( function (data) {	// Sem  Error desde o Servidor
					
                    // fechamos Icono de Procesamiento
        			$scope.dialogLoadHide('dialog-1');
					
					console.log('data server' , data);
					
					//insere o novo item
					$scope.$storage.items[$scope.$storage.index] = $scope.$storage.Item;
					
					// fecha o modal
					$scope.dialogLoadHide('myModal');
				 
                
				},
			    function (err) {	// Com Error desde o Servidor 
                    
                    // fecha o modal
					$scope.dialogLoadHide('myModal');
					
					// fechamos Icono de Procesamiento
					$scope.dialogLoadHide('dialog-1');
					
					// Carregamos mensagem para o alert
					$scope.aviso = {};
					$scope.aviso.titulo = 'Error';
					$scope.aviso.mensaje = 'Error tentando Inserir';
					
					// Mostramos o alert
					$scope.alert($scope.aviso);
                    
				});
			}
		
	}
	
	// função que mostra um alert para que o usuario inseri a nova 'Marca' que vai atualizar um veiculo
	$scope.getMarcaVeiculo = function(id){
		 
		 ons.notification.prompt({
		    message: 'Nova Marca',
		    buttonLabels:['Fechar','Atualizar'],
		    callback: function(marca) {
		    	
		    	// valida se inserio algo o não cancelo 
		    	if(marca){ 
			    	// valida que o usuaria haja inserido algo
			    	if(marca.length > 0){
			        	$scope.updateMarcaVeiculo(marca,id);
			    	}
			    	else{ // se o usuario não inseriu nada se mostrar o alert
			    		$scope.aviso = {};
						$scope.aviso.titulo = 'Error';
						$scope.aviso.mensaje = 'Marca Não pode ser null';
						
						// Mostramos alert
						$scope.alert($scope.aviso);
			    	}
		    	}
		    	
		    }
		}); 
	}
	
	// função que atualiza a 'Marca' dum veiculo 
	$scope.updateMarcaVeiculo = function(marca,id){
			
		// valida que exista a 'marca' e ID do veiculo a atualizar
		if (marca && id) {
	
			// Mostramos Icono de Procesamento
			$scope.dialogLoadShow('dialog-1',':)');
			
			var requestUrl = '/veiculos/' + id ;
	
			$http.patch(requestUrl, {marca: marca})
			.then( function (data) {	// Sem Error desde o Servidor
				
                // Fechamos Icono de Procesamento
    			$scope.dialogLoadHide('dialog-1');
				
				//insere o novo item
				$scope.$storage.items[$scope.$storage.index] = data.data.item;
				
				// atuliza item escolhido
				$scope.itemSelect = data.data.item;
			 
            
			},
		    function (err) {	// Com Error desde o Servidor
                
                
				
				// Fechamos Icono de Procesamento
				$scope.dialogLoadHide('dialog-1');
				
				// Carregamos mensagem para o alert
				$scope.aviso = {};
				$scope.aviso.titulo = 'Error';
				$scope.aviso.mensaje = 'Error tentando Inserir';
				
				// Mostramos o alert
				$scope.alert($scope.aviso);
                
			});
		}
		
	}

		
});

var express = require('express');
var router = express.Router();

//*****************************************************************************************//
//		Bibliotecas
//*****************************************************************************************//

//---------------------------------------//
//		Externas
//---------------------------------------//

// gerador de UUID
var uuid = require('shortid');


//*****************************************************************************************//
//		Serviços Nuvem
//*****************************************************************************************//

//---------------------------------------//
//		Log Management
//---------------------------------------//

// Logentries
var LogentriesClient = require('logentries-client');

// Credenciais Log Back_end
var log = new LogentriesClient({
    token: 'xxx',
	timestamp : true,
	withStack : true
});

// Dirigimos Error de Logentries 
log.on('error',function(err){ 
	console.log('LE ERROR', err);
});


//*****************************************************************************************//
//		DB-Link
//*****************************************************************************************//

//------------------------------------------------//
//		MongoDB
//------------------------------------------------//

// Bring Mongoose into the project
var mongoose = require( 'mongoose' );

// Credenciais de (BD) en MongoLab - (Test)
var dbURI = 'xxx';	

// Create the database connection
mongoose.connect(dbURI);

//-----------------------//
//	  Events Mongoose
//-----------------------//

// Conexion MongoDB
mongoose.connection.on('connected', function () {
	
	// Guardamos evento em Log
	log.log('info', {source : 'BD', 
					 BD : 'MongoDB' ,
					 funcion : 'mongoose.connection.on' , 
					 evento : 'Mongoose connected to ' + dbURI});	
});

// Error em Conexion MongoDB
mongoose.connection.on('error',function (err) {	
	
	// Guardamos Error em Log
	log.log('err', {source : 'BD', 
					BD : 'MongoDB' ,
					funcion : 'mongoose.connection.on' , 
					evento : 'Mongoose connection error: ' , 
					error : err});	
		
	// Re-Conexion MongoDB
	mongoose.connection.on('connected', function () {
		
		// Guardamos evento no Log
		log.log('info', {source : 'BD', 
						 BD : 'MongoDB' ,
						 funcion : 'Re-Conexion MongoDB' , 
						 evento : 'Mongoose connected to ' + dbURI});	
	});
					
});


//------------------------------------------//
//			Define - User schema 
//------------------------------------------//

// Esquema Usuario
var carSchema = new mongoose.Schema({

	veiculo	:	String,
	marca	:  	String,
	ano		:	Number,
	descricao:	String,
	vendido	:	Boolean
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'  }});

//------------------------------------------//
//			Build the Car model 
//------------------------------------------//

var car_info = mongoose.model( 'car_info', carSchema );


//*****************************************************************************************//
//		Fin execução APP
//*****************************************************************************************//

//------------------------------------------------//
//	  Eventos NodeJS
//------------------------------------------------//

// Fim de execução con [CTRL+C]
process.on('SIGINT', function() { 
	
	// Guardamos Fim de App en Log
	log.log('notice', {source : 'NodeJS', 
					  funcion : 'SIGINT' , 
					  evento : 'app termination'});	
	
	console.log('app termination');
	
	// fechamos Cliente Mongoose
	mongoose.connection.close();
	
	// Delay de 2 Seg.
	setTimeout( function() { 
		
		// fechamos conexion Logentries
		log.end();
		
		// Code Exit (0) - Ok
		process.exit(0); 
		
	} , 2000);

});

// Fim de execução por Error Sem manejador
process.on('uncaughtException', function (err) {
	
	// Guardamos Error em Log
	log.log('err', {source : 'NodeJS', 
					funcion : 'uncaughtException' , 
					evento : 'Errores Sin manejador' , 
					error : err});	
	
	console.log('app termination uncaughtException');
	
	// fechamos Cliente Mongoose
	mongoose.connection.close();
	
	// Delay de 2 Seg.
	setTimeout( function() { 
	
		// fechamos conexion Logentries
		log.end();
		
		// Code Exit (1) - (Uncaught Fatal Exception)
		process.exit(1); 
		
	} , 2000);
  
});



//*****************************************************************************************//
//		Inicio de APP
//*****************************************************************************************//

console.log('app start');

// Log Inicio App
log.log('info', { source : 'App - pru', 
				  evento : 'app start'});	


//*****************************************************************************************//
//		Logica
//*****************************************************************************************//

//--------------------------------------------------------------//
//		funções 
//--------------------------------------------------------------//

router.allVeiculos = function(req, res){	// getAllVeiculos

	
	// pesquisamos Items
	car_info.find( {} 
	,function(err,items) {
		
		if (!err) {	// Sim Não error (BD) - MongoDB
		
			if (items.length == 0) {	// Sem items obtidos
				
				// Resposta ao Front
				res.status(200).send({ error: 'msg_code_17', items: [] });
							
			}else{	// Com items obtidos
				
				// Resposta ao Front
				res.status(200).send({ message: 'msg_code_16' , items: items });
			
			}
		
		}else{	// Com error (BD) - MongoDB
		
			// Guardamos Error em Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'busqItems' , 
							evento 	: 'pesquisamos Items' , 
							error 	: err});	
			
			// Respuesta a Front
			res.status(400).send({ error: 'msg_code_17'});
			
		}
		
	});
	
};

router.idVeiculo = function(req, res){	// Buscamos Items pelo ID

	// Buscamos Items pelo ID
	car_info.find( {"_id" : req.params.id}
	,function(err,item) {
		
		if (!err) {	// Sem error (BD) - MongoDB
		
			if (item.length == 0) {	// Item Não obtido
				
				// Respuesta al Front
				res.status(200).send({ error: 'msg_code_17', item: [] });
							
			}else{	// items obtido
				
				// Respuesta ao Front
				res.status(200).send({ message: 'msg_code_16' , item: item[0] });
			
			}
		
		}else{	// Com error (BD) - MongoDB
		
			// Guardamos Error no Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'busqItems' , 
							evento 	: 'Buscamos Items pelo ID' , 
							error 	: err});	
			
			// Respuesta a Front
			res.status(400).send({ error: 'msg_code_17'});
			
		}
		
	});
	
};

router.findVeiculos = function(req, res){	// Pesquisamos Veiculos por Text
	
	// Pesquisamos Veiculos por Text
	car_info.find({ $text:
	    {
	      $search: req.query.q
	    }
	 }
	,function(err,items) {
		
		if (!err) {	// Sem error (BD) - MongoDB
		
			if (items.length == 0) {	// nenhum item obtido
				
				// Respuesta ao Front
				res.status(200).send({ error: 'msg_code_17', items: [] });
							
			}else{	// algum items obtido
				
				// Respuesta ao Front
				res.status(200).send({ message: 'msg_code_16' , items: items });
			
			}
		
		}else{	// Com error (BD) - MongoDB
		
			console.log(err);
			
			// Guardamos Error no Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'busqItems' , 
							evento 	: 'Pesquisamos Items por Text' , 
							error 	: err});	
			
			// Respuesta ao Front
			res.status(400).send({ error: 'msg_code_17'});
			
		}
		
	});
	
};

router.addVeiculo = function(req, res){	// Adiccionar item

 	var veiculo			= req.body.veiculo;		// pegamos [veiculo]
 	var marca			= req.body.marca;		// pegamos [marca]
 	var ano				= req.body.ano;			// pegamos [ano]
 	var descricao 		= req.body.descricao;	// pegamos [descricao]
	var vendido			= req.body.vendido;		// pegamos [vendido]

	// Os dados do formulário são validados
	if (   null == veiculo 		|| veiculo.length < 1 
		|| null == marca 		|| marca.length < 1 
		|| null == ano 			|| ano.length < 1 
		|| null == descricao 	|| descricao.length < 1 
		|| null == vendido		|| vendido.length < 1 
		) {
		
		res.status(400).send({ error : 'Error Datos del Formulario' });
		return;
	}
	
 

	// Criamos o item em MongoDB
	car_info.create({
	  veiculo	: 	veiculo,
	  marca		: 	marca,
	  ano		: 	ano,
	  descricao	: 	descricao,
	  vendido	: 	vendido
	  
	},function(err,item) {
		
		if (!err) {	// Sem error (BD) - MongoDB
		
			if (item) {	// Se item Creado
				
				// Respuesta al Front
				res.status(200).send({ message: 'msg_code_2' , item  : item });
			}
			
		}else{	// Com error (BD) - MongoDB
		
			// Guardamos Error no Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'addItem' , 
							evento 	: 'Adiccionar item' , 
							error 	: err});	
			
			// Respuesta ao Front
			res.status(400).send({ error: 'Error'});
			
		}
	});
	
};

router.putVeiculo = function(req, res){	// Atualização item

 	var _idItem			= req.params.id;		// pegamos [_idItem]
 	var veiculo			= req.body.veiculo;		// pegamos [veiculo]
 	var marca			= req.body.marca;		// pegamos [marca]
 	var ano				= req.body.ano;			// pegamos [ano]
 	var descricao 		= req.body.descricao;	// pegamos [descricao]
	var vendido			= req.body.vendido;		// pegamos [vendido]

	// Os dados do formulário são validados
	if (   null == _idItem 		|| _idItem.length < 1 
		|| null == veiculo 		|| veiculo.length < 1 
		|| null == marca 		|| marca.length < 1 
		|| null == ano 			|| ano.length < 1 
		|| null == descricao 	|| descricao.length < 1 
		|| null == vendido		|| vendido.length < 1 
		) {
		
		res.status(400).send({ error : 'Error Datos del Formulario' });
		return;
	}
	
	// pesquisamos Item a actualizar por ID
	car_info.findByIdAndUpdate( _idItem , 
	{
	  veiculo	: 	veiculo,
	  marca		: 	marca,
	  ano		: 	ano,
	  descricao : 	descricao,
	  vendido 	: 	vendido
	},
	
	{new : true}	// Retorna o Document Atualizado
	
	,function(err,item) {
		if (!err) {	// Sem error (BD) - MongoDB
			if (item) {	// Se item Atualizado
				
				// Respuesta ao Front
				res.status(200).send({ message: 'msg_code_16' });
							
			}else{	// Se item No actualzado
			
				// Respuesta ao Front
				res.status(400).send({ error: 'msg_code_17'});
				
			}
		
		}else{	// Com error (BD) - MongoDB
		
			// Guardamos Error no Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'modItem' , 
							evento 	: 'Atualização item' , 
							error 	: err});	
			
			// Respuesta ao Front
			res.status(400).send({ error: 'msg_code_17'});
			
		}
		
	});
	
};


router.patchVeiculo = function(req, res){	// Atualização Marca do Veiculo

 	var _idItem	= req.params.id;	// pegamos [_idItem]
 	var marca	= req.body.marca;	// pegamos [marca]
	
	// Os dados do formulário são validados
	if (   null == _idItem	|| _idItem.length < 1 
		|| null == marca	|| marca.length < 1 
		) {
		
		res.status(400).send({ error : 'Error Datos del Formulario' });
		return;
	}
	
	// Atualizamos a Marca do veiculo 
	car_info.findByIdAndUpdate( _idItem , 
	{
	  marca		: 	marca
	},
	
	{new : true}	// Retorna o Document Atualizado
	
	,function(err,item) {
		if (!err) {	// Sem error (BD) - MongoDB
			if (item) {	// Se item Atualizado
				
				// Respuesta ao Front
				res.status(200).send({ message: 'msg_code_16', item  : item });
							
			}else{	// Se item No actualzado
			
				// Respuesta ao Front
				res.status(400).send({ error: 'msg_code_17'});
				
			}
		
		}else{	// Com error (BD) - MongoDB
		
			// Guardamos Error no Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'modItem' , 
							evento 	: 'Atualização item' , 
							error 	: err});	
			
			// Respuesta ao Front
			res.status(400).send({ error: 'msg_code_17'});
			
		}
		
	});
	
};

router.deleteVeiculo = function(req, res){	// Eliminar Item

 	var _idItem 	 = req.params.id;	// pegamos [_idItem]

	// Os dados do formulário são validados
	if ( 	null == _idItem  	 || _idItem.length < 1 ) {
		
		res.status(400).send({ error : 'Error Datos del Formulario' });
		return;
	}
	
	
	// Eliminamos o [Item] de MongoDB
	car_info.findByIdAndRemove( _idItem , function(err, item) { 
		if (!err) {	// Sem error (BD)
			
			res.send(200,{ code:200, message: 'Item Deleted' });
			
		}else{	// Com error (BD)
		
			// Guardamos Error en Log
			log.log('err', {source 	: 'DB', 
							BD 		: 'MongoDB' , 
							funcion : 'deleteItem' , 
							evento 	: 'Eliminar Item' , 
							error 	: err});	
			
			// Respuesta al Front
			res.status(400).send({ error: 'Error'});
			
		}
	});
	
};
	
	
//----------------------//
//----------------------//

module.exports = router;